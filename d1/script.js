console.log ("*** JS ARRAYS ***")

/*
Arrays - used to store multiple related values in a single variable
- declared using square brackets [] known as array literals

Elements - are values inside an array

Index always starts at zero
0 = offset
0 coordinate = first element

Element 1 = index 0

Syntax:

let/const arrayName = [elementA, elementB, elementC]

*/

let grades = [98, 94, 89, 90]; //number array
let computerBrands = ['Acer', 'Asus', 'Lenovo', 'Neo', 'Redfox', 'Gateway', 'Toshiba', 'Fujitsu'] //string array

let mixedArr = [12, 'Asus', null, undefined, {}] //not recommended
console.log(mixedArr);


//Reading an Array
console.log(grades[0]); //98

console.log(computerBrands[3]); //Mac

console.log(grades[10]); //undefined

let myTasks = [
    'bake sass',
    'drink html',
    'inhale css',
    'eat javascript'
];

console.log (myTasks);



//Reassigning Value

myTasks[0] = 'hello world';
console.log(myTasks);

//Getting the length of an array
console.log(computerBrands.length);

// let lastIndex = myTasks.length -1;
// console.log (lastIndex);


/*
ARRAY METHODS

1. Mutator Methods
-are functions that "mutate" or change an array
*/

//push 
//-- adds an element in the end of the array and returns its length
//syntax: arrayName.push(elementA, elementB)

let fruits = ['Apple', 'Orange', 'Kiwi', 'Dragon Fruit'];
console.log ('Current Array');
console.log (fruits);
console.log ('Array after push()');

let fruitsLength = fruits.push ('Mango');
console.log (fruits);
console.log (fruitsLength);

let fruitsLength2 = fruits.push ('Lanzones');
console.log (fruits);
console.log (fruitsLength2);
console.log (fruits.length);

// let fruitsLength4 = fruits.push ('Banana', 'Coconut');
// console.log (fruits);
// console.log(fruits.length);
// console.log(fruitsLength4);


let fruitsLength3 = fruits.push ('Guava', 'Avocado');
console.log ('Mutated array after push()');
console.log (fruits);


//pop
// removes the last element in an array and return the removed element

let removedFruit = fruits.pop();
console.log ('Mutated array after pop()');
console.log(fruits);
console.log(removedFruit);

//shift
//-removes an element at the beginning of an array and returns the removed element

let firstFruit = fruits.shift();
console.log('Mutated array after shift()');
console.log(fruits);
console.log(firstFruit);


//unshift
//-adds one or more element/s at the beginning of an array

fruits.unshift ('Lime', 'Banana');
console.log ('Mutated array after unshift()');
console.log(fruits);

//splice
//-simultaneously removes elements from specified index and adds new elements 
//syntax; arrayName.splice(startingIndex, deleteCount, elements to add)

fruits.splice (4); //deleted element 4 and onwards
console.log (fruits);
fruits.splice(2,2);
console.log(fruits);
fruits.splice(1,1, 'Lemon','Grapes');
console.log(fruits);
fruits.splice (0, 1, 'A', 'B', 'C');
console.log(fruits);
fruits.splice (1, 1, 'D', 'E', 'F');
console.log(fruits);
fruits.splice (2, 2, 'J', 'K', 'L');
console.log(fruits);
let sort = fruits.sort();
console.log(sort);

// fruits.splice (1); //banana deleted
// console.log(fruits);
// fruits.splice (2, 2); //deletes index 2 and 3 (deletes starting from index 2 and deletes 2 items/counts) //deleted kiwi and dragonfruit
// console.log(fruits);
// fruits.splice (1, 2, 'Cherry', 'Grapes') //deleted orange, mango ; added cherry and grapes
// console.log('Mutated array after splice()');
// console.log(fruits);

// //sort - rearranges the elements in alphanumeric order
// fruits.sort();
// console.log('Mutated array after sort()');
// console.log(fruits);

// //reverse - reverses the order of an array
// fruits.reverse();
// console.log('Mutated array after sort()');
// console.log(fruits);


/*
2. Non Mutator Methods
-are functions that do not modify or change the array
*/

let countries = ['US', 'PH', 'CAN', 'SG', 'TH', 'PH', 'FR', 'DE'];

//indexOf()
//-returns the first index of the first matching element found in an array
//if no match is found, it returns -1 (can be used of if else statement == -1)

let firstIndex= countries.indexOf('PH');
console.log('Result of indexOf() method:' + firstIndex);

let invalidCountry = countries.indexOf ('BR');
console.log('Result of indexOf() method:' + invalidCountry);

//lastIndexOf()
//-returns the last matching element found in an array
//syntax: arrayName.lastIndexOf (searchValue);
//arrayName.lastIndexOf (searchValue, fromIndex);


let lastIndex= countries.lastIndexOf('PH');
console.log('Result of lastIndexOf() method:' + lastIndex);


let lastIndexStart= countries.lastIndexOf('PH', 2);
console.log('Result of lastIndexOf() method:' + lastIndexStart);

//slice 
//-slices a portion of an array and returns a new array
//syntax arrayName.slice(startingIndex);
//syntax arrayName.slice(startingIndex, endingElement);

console.log(countries);

let slicedArrayA = countries.slice(2);
console.log ('Result from slice method A');
console.log(slicedArrayA);

let slicedArrayB = countries.slice(2, 4);
console.log ('Result from slice method B');
console.log(slicedArrayB);

let slicedArrayC = countries.slice(-3);
console.log ('Result from slice method C');
console.log(slicedArrayC);


//toString
//-makes an array to string and separated by commas

let stringArray = countries.toString ();
console.log ('Result from to string()');
console.log (stringArray);

//concat - combines two or more arrays and return the combined result

let tasksArrayA = ['drink HTML', 'eat Javascript'];
let tasksArrayB = ['inhales CSS', 'breath sass'];
let tasksArrayC = ['get git', 'be node'];

let tasks = tasksArrayA.concat (tasksArrayB);
console.log ('Result from concat()');
console.log(tasks);

let allTasks = tasksArrayA.concat (tasksArrayB, tasksArrayC);
console.log ('Result from concat()');
console.log(allTasks);

let combinedTasks = tasksArrayA.concat ('smell express', 'throw react');
console.log ('Result from concat()');
console.log(combinedTasks);


//join - returns an array as string separated by specified separator

let users = ['John', 'Jane', 'Joe', 'Robert'];
console.log(users.join());
console.log(users.join(''));
console.log(users.join(' - '));
console.log(users.join(' & '));


/*
3. Iteration Methods
-loops designed to perform repetitive tasks on arrays

*/


//foreach 
//-calls an function for each element in an array
//syntax:
/*arrayName.forEach(function(individualElement)){statement})*/

console.log (countries);

countries.forEach(function(country){
    console.log(country);
})

console.log(allTasks);

allTasks.forEach (function(task){
    console.log(task);
})


let filteredTasks = [];

allTasks.forEach (function (task){
    if (task.length > 10){
        filteredTasks.push(task);
    }
})

console.log('Result from forEach()');
console.log(filteredTasks);

//map - iterates on each element and returns new array with different values depending on the results of the function's operation

let numbers = [1, 2, 3, 4, 5];

let numberMap = numbers.map(function(number){
    return number * number;
})

console.log('Result from map()');
console.log(numberMap);


let numberMap2 = [];


numbers.forEach (function (number){
    let square = number * number;
    numberMap2.push(square);
})

console.log('Result from forEach()');
console.log(numberMap2);



let arrayMap = allTasks.map(function(task){
    return "I " + task;
})

console.log('Result from arrayMap()');
console.log(arrayMap);


//every 

let allValid = numbers.every (function(number){
    return (number < 3);
})

console.log ('Result from every ()');
console.log (allValid);


//some

let someValid = numbers.some (function(number){
    return (number < 3);
})

console.log ('Result from some()');
console.log (someValid);


//filter - returns a new array that meets the given condition

let filterValid = numbers.filter(function(number){
    return (number < 3);
})

console.log ('Result from filter()');
console.log (filterValid);


//includes

let products = ['Mouse', 'Keyboard', 'Laptop', 'Monitor', 'Apple'];

let filterProducts = products.filter(function(product){
    return product.toLowerCase().includes('a')
})

console.log ('Result from filter() & includes');
console.log (filterProducts);



// reducing string arrays
let list = ['Hello', 'Again', 'World'];

let reducedJoin = list.reduce(function(x, y) {
    return x + ' ' + y;
});
console.log("Result of reduce method: " + reducedJoin);

//reducing stray using numbers:
const array1 = [1, 2, 3, 4];
const reducer = (previousValue, currentValue) => previousValue + currentValue;


// 1 + 2 + 3 + 4
console.log(array1.reduce(reducer));
// expected output: 10

// 5 + 1 + 2 + 3 + 4
console.log(array1.reduce(reducer, 5));
// expected output: 15

const array = [15, 16, 17, 18, 19];

function reducer2(previous, current, index, array) {
  const returns = previous + current;
  console.log(`previous: ${previous}, current: ${current}, index: ${index}, returns: ${returns}`);
  return returns;
}

array.reduce(reducer2);

//Multi-dimensional Array - reading from row then column (using index)

let chessBoard = [
    ['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'],
    ['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
    ['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
    ['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
    ['a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5'],
    ['a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'g6', 'h6'],
    ['a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'g7', 'h7'],
    ['a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8']
];

console.log(chessBoard);
console.log(chessBoard[1][4]); //e2 (row then column)
console.log('Pawn moves to f2: ' + chessBoard[1][5]);
console.log('Knight moves to c7: ' + chessBoard[6][2]);


console.log("Arrianne")
